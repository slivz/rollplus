local addonName, addon = ...
function addon:GetPlayerName(target)
	-- if they have an rp name, use that instead
	if TRP3_API then --total rp 3
		local name = self:GetPlayerNameTRP3(target)--TRP3_API.r.name(target)
		if name then
			-- make use of the trp3 color if its set
			local col = self:GetPlayerColor(target)
			if col and col ~= "" then
				name = "|cFF"..col..name.."|r"
			end
			return name
		end
	elseif mrp then --myroleplay
		local supported = msp.char[target].supported
		if supported then
			return msp.char[target].field.NA
		end
	end
	return target
end

function addon:GetPlayerNameTRP3(target)
	local unitProfile
	local unitID = TRP3_API.utils.str.getUnitID(target) or target
	if unitID == TRP3_API.globals.player_id then
		unitProfile = TRP3_API.profile.getPlayerCurrentProfile()
		if unitProfile and unitProfile.player and unitProfile.player.characteristics then
			local c = unitProfile.player.characteristics
			local FN, LN = c.FN or "", c.LN or ""
			if FN and LN then
				return string.format("%s %s", FN, LN)
			elseif LN then
				return LN
			end
			return FN
		end
	elseif TRP3_API.register.isUnitIDKnown(unitID) then
		unitProfile = TRP3_API.register.getUnitIDProfile(unitID)
		if unitProfile and unitProfile.characteristics then
			local c = unitProfile.characteristics
			local FN, LN = c.FN or "", c.LN or ""
			if FN and LN then
				return string.format("%s %s", FN, LN)
			elseif LN then
				return LN
			end
			return FN
		end
	end
	return unitID:gsub("-.+", "")
end

-- cheers EllypseCelwe for the help with this function
function addon:GetPlayerColor(target)
	local unitProfile
	local unitID = TRP3_API.utils.str.getUnitID(target) or target
	if unitID then
		-- trp3 handles the local player somewhat differently
		-- so we too will handle this case differently
		if unitID == TRP3_API.globals.player_id then
			unitProfile = TRP3_API.profile.getPlayerCurrentProfile()
			if unitProfile and unitProfile.player and unitProfile.player.characteristics and unitProfile.player.characteristics.CH then
				return unitProfile.player.characteristics.CH
			end
		elseif TRP3_API.register.isUnitIDKnown(unitID) then
			unitProfile = TRP3_API.register.getUnitIDProfile(unitID)
			if unitProfile and unitProfile.characteristics and unitProfile.characteristics.CH then
				return unitProfile.characteristics.CH
			end
		end
	end
	return nil
end

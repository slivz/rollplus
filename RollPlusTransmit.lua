local addonName, addon = ...
function addon:Transmit(cmd, text)
	-- verify args exist
	if cmd and text then
		-- its basic but it works. may try some fancy encoding @ some point
		local msg = string.format("{%s}{%s}", cmd, text)
		local channelID = GetChannelName(addonName..GetZoneText():gsub('%s', ''))
		ChatThrottleLib:SendAddonMessage("ALERT", "RPLUS", msg, "CHANNEL", channelID)
	end
end

function addon:IncomingMessage(prefix, message, channel, sender)
	-- if this message is coming from this addon
	if prefix == "RPLUS" then
		local senderRealm, playerRealm = sender:match("%-(.+)"), GetRealmName():gsub('%s', '')
		local cmd, text = message:match("{(.+)}{(.+)}")
		-- TODO: check proximity of sender prior to output
		self:Output(sender, cmd, text)
	end
end
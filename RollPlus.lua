local addonName, addon = ...
addon.Operators = {
	["+"] = function(a, b) return a + b end,
	["-"] = function(a, b) return a - b end,
	["*"] = function(a, b) return a * b end,
	["/"] = function(a, b) return a / b end,
}

-- basic rounding function
local function Round(x)
	local decimal = x % 1
	if decimal >= 0.5 then
		return math.ceil(x)
	else
		return math.floor(x)
	end
end

-- essentially turn a table of numbers into a string for display
-- may do away with this in the future, and handle it on receive
local function FormatTable(tbl, sides)
	local str = ""
	for k,v in pairs(tbl) do
		if v == tonumber(sides) then
			str = str .. "|cFF00FF00" .. v .. "|r "
		elseif v == 1 then
			str = str .. "|cFFFF0000" .. v .. "|r "
		else
			str = str .. v .. " "
		end
	end
	return string.format("(%s)", string.sub(str, 0, -2))
end

-- roll function
local function Roll(dice, sides)
	local res = {}
	local total = 0
	for i=1, dice do
		local val = Round(math.random() * sides + 0.5)
		res[i] = val
		total = total + val
	end
	return res, total
end

-- output data to chatbox
function addon:Output(player, cmd, text)
	if DEFAULT_CHAT_FRAME then
		local name = self:GetPlayerName(player)
		DEFAULT_CHAT_FRAME:AddMessage(string.format("%s rolls %s", name, cmd), 1, 1, 1)
		DEFAULT_CHAT_FRAME:AddMessage(text, 1, 1, 1)
	end
end

-- pattern matching!
function addon:ParseCommand(str)
	-- e.g. "1d20+5"
	local rolls = string.gmatch(str, "[%+%-%*%/]?%s?%d+[dD]?%d*")
	local count, total = 1, 0
	local cmd, text = "", ""
	
	for v in rolls do
		local dice, sides = v:match("(%d+)[dD]?(%d*)")
		local op = v:match("([%+%-%*%/])%s?%d+") or "+"
		if tonumber(sides) then
			local tbl, val = Roll(dice, sides)
			if text ~= "" then
				cmd = cmd .. op
				text = text .. op
			end
			cmd = cmd .. dice .. "d" .. sides
			text = text .. FormatTable(tbl, sides)
			total = self.Operators[op](total, val)
		else
			if text ~= "" then
				cmd = cmd .. op
				text = text .. op
				v = string.sub(string.gsub(v, "%s", ""), 2, -1)
			end
			cmd = cmd .. v
			text = text .. v
			total = self.Operators[op](total, v)
		end
		count = count + 1
	end
	return cmd, string.format("%s = %s", text, total)
end

SLASH_ROLLPLUS1 = '/rp'
SLASH_ROLLPLUS2 = '/rollplus'
SlashCmdList["ROLLPLUS"] = function(msg, editbox)
	if msg:gsub('%s', '') ~= "" then
		local cmd, text = addon:ParseCommand(msg)
		if cmd ~= "" then
			addon:Transmit(cmd, text)
			return
		end
	end
	print("Syntax: [# rolls]d[# sides] [+/-] [#]/[# rolls]d[# sides] [+/-] ...")
end
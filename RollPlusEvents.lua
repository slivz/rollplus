local addonName, addon = ...
local channelSetup = false
function RollPlus_EventHandler(self, event, ...)
	if event == "CHAT_MSG_ADDON" then
		-- pass message to a function that can deal with it
		addon:IncomingMessage(...) 
	elseif event == "PLAYER_ENTERING_WORLD" then
		-- aa
	elseif event == "CHAT_MSG_CHANNEL" or event == "CHAT_MSG_CHANNEL_NOTICE" then
		-- this is done so we dont join our channel before general/trade/etc
		-- its works for now, will see if can be replaced down the line
		if not channelSetup then
			local channelName = addonName..GetZoneText():gsub('%s', '')
			JoinChannelByName(channelName)
			channelSetup = true
		end
	elseif event == "ZONE_CHANGED_NEW_AREA" then
		-- we have entered a new area, leave previous chat channel and join new one
		local channels = {GetChannelList()}
		for k,v in pairs(channels) do
			if type(v) == "string" then
				if v:match("^"..addonName..".+") then
					LeaveChannelByName(v)
				end
			end
		end
		local channelName = addonName..GetZoneText():gsub('%s', '')
		JoinChannelByName(channelName)
		channelSetup = true
	end
end

function RollPlus_LoadHandler()
	succ = RegisterAddonMessagePrefix("RPLUS")
	addon.__loaded = succ
end

function RollPlus_UpdateHandler(self, elapsed)
	-- this function is called very frequently.
	-- will be used to make a timer if need be
end

-- dummy frame so we can watch for events
addon.DummyFrame = addon.DummyFrame or CreateFrame("Frame", "RollPlus_DummyFrame")
addon.DummyFrame:SetScript("OnEvent", RollPlus_EventHandler)
addon.DummyFrame:SetScript("OnUpdate", RollPlus_UpdateHandler)
addon.DummyFrame:RegisterEvent("CHAT_MSG_ADDON")
addon.DummyFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
addon.DummyFrame:RegisterEvent("CHAT_MSG_CHANNEL")
addon.DummyFrame:RegisterEvent("CHAT_MSG_CHANNEL_NOTICE")
addon.DummyFrame:RegisterEvent("ZONE_CHANGED_NEW_AREA")
RollPlus_LoadHandler()